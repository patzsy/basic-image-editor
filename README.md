# basic-image-editor


Tested on Chrome 79.0.3945.130, Safari 13.0.4, Firefox 72.0.2 (64-bit), iPhone 11 Pro(IOS 13.3).

## Getting started

``` bash
# install dependencies
> npm install
# serve with hot reload at localhost:8080
> npm run serve
```

Other commands available are:

``` bash
# build for production with minification
> npm run build
```
